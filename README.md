# README

Repo con la presentación de la charla sobre el Fediverso en la Devroom
de Trackula en esLibre 2019

## Licencia

Esta presentación se comparte con licencia GPL v3.0. El contenido
creado por mí lleva licencia CC-BY SA.

## Imágenes utilizadas

* [Logo Fediverso color](https://commons.wikimedia.org/wiki/File:Fediverse_logo_proposal.svg) CC0 (Dominio Público)
* [Logo Fediverso mono](https://commons.wikimedia.org/wiki/File:Fediverse_logo_proposal_(mono_version).svg) CC0 (Dominio Público)
* [Logo Mastodon](https://commons.wikimedia.org/wiki/File:Mastodon_Logotype_(Simple).svg)  GNU Affero General Public License 

Para el resto de las imágenes no he podido encontrar un autor/licencia, les pertenecen a ellos.
